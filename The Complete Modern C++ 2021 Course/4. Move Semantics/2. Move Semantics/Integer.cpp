#include "Integer.h"
#include <iostream>
Integer::Integer() // Default construtor 
{
	std::cout << "Integer()" << std::endl;
	m_pInt = new int(0); // Allocate memory to zero
}

Integer::Integer(int value) // Parametized construtor
{
	std::cout << "Integer(int value)" << std::endl;
	m_pInt = new int(value); // Allocate for the user define value 
}

Integer::Integer(const Integer& obj) //passing the obj by refenece ! |||| DEEP COPY  | copy constructor .!!!!!  
{
	std::cout << "Integer(const Integer& obj)" << std::endl;
	m_pInt = new int(*obj.m_pInt); // DEEP COPY
}

Integer::Integer(Integer&& obj)
{
	std::cout << "Integer(Integer&& obj)" << std::endl;
	m_pInt = obj.m_pInt;   // Shallow copy
	obj.m_pInt = nullptr; // Stolen the rescoes from the obj and assing null.
}

int Integer::GetValue() const
{
	return *m_pInt;
}

void Integer::SetValue(int value)
{
	*m_pInt = value;
}

Integer::~Integer() // Deconstructor.
{
	std::cout << "~Integer()" << std::endl;
	delete m_pInt; // free the memory that was allocated by the pointer
}
