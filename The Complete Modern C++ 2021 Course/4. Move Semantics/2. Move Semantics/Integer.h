#pragma once
class Integer {
	int* m_pInt; // Pointer as member 
public:
	Integer();						// Default construtor				
	Integer(int value);				// Parametized construtor			
	Integer(const Integer& obj);	// DEEP COPY  | copy constructor!	
	Integer(Integer&& obj);			// Shallow copy	| Move  consturtor					
	int GetValue()const;			// Function 
	void SetValue(int value);		// Function
	~Integer();						// Destructor						
};
