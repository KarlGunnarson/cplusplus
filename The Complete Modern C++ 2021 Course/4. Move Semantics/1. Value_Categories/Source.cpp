#include <iostream>
/*
L-Value has a name
L-Value can be assign a value !
Functions that return by reference return L-Value



R-Value does not have a name ! 
R-Value can be a expresiton
R-Value is Temporary.!
Functions that return by value return R-value.
R-Value can be connected to const reference in function  !
Temporary will always bind to R-Value refreneces and also const references ! 

R-VALUE REFERENCES 
&& OPERATOR ! 
*/

// Returns r-value
int Add(int x, int y) {
	return x + y;
}

//Returns l-value
// Retrun by reference ! 
int& Transform(int& x) {
	x *= x;
	return x;
}


void Print(int &x) {
	std::cout << "Print(int& x)" << std::endl;
}

void Print(const int& x) {
	std::cout << "Print(const int& x)" << std::endl;
}


void Print( int&& x) { // Function that accept r-value reference
	std::cout << "Print( int&& x)" << std::endl;
}

int main() {
	int&& r1= 10; // R-value reference
	int y{ 19 };
	
	int& l1 = y; // L-Value refereence.
	
	int x{ 10 };
	Print(x);
	Print(87);

}