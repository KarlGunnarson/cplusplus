#include "Integer.h"
#include <memory>


class Project {
public:
	~Project() {
		std::cout << "~Project()" << std::endl;
	}
};

class Employee {
	std::shared_ptr<Project> m_pProject;

public:
	void setProject(std::shared_ptr<Project> p) {
	m_pProject = p ;
	}
	
	std::shared_ptr<Project> GetProject()const {
		return m_pProject;
	}

	~Employee() {
		std::cout << "~Employee()" << std::endl;
	}
};

std::shared_ptr<Employee> AssignProject() {
	std::shared_ptr<Project> p{ new Project{} };
	Employee* e1 = new Employee{};
	e1->setProject(p);
	return std::shared_ptr<Employee>{e1};
}
int main() {
	auto sp = AssignProject();
	if (sp == nullptr) {

	}

	sp.get();
	sp.reset(new Employee);
	return 0;
}