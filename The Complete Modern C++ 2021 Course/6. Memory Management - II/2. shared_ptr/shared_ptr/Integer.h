#pragma once
#include <iostream>
class Integer {
	int* m_pInt; // Pointer as member 
public:
	Integer();						// Default construtor				
	Integer(int value);				// Parametized construtor			
	Integer(const Integer& obj);	// DEEP COPY	| copy constructor!	| passed by reference | lvalue
	Integer(Integer&& obj);			// Shallow copy	| Move  constructor	| R-value Reference				
	int GetValue()const;			// Function 
	void SetValue(int value);		// Function
	~Integer();						// Destructor		

	// Operator overloading 
	Integer operator +(const Integer& a)const;
	Integer& operator ++();						// Return type by reference
	Integer operator ++(int);					// Return type by temp variable
	Integer& operator =(const Integer& a);		// Deep copy ! 
	Integer& operator =(Integer&& a);			// Move assignment | R-value
	void operator()();
	friend std::istream& operator >>(std::istream& input, Integer& a); // Friend function should be last result of trying to solve problems 
	friend class Printer;

	// Type conversion part 3  User-defined to primitive
	explicit operator int();
};

class Printer {

};
