#include <memory>
#include <iostream>

class Employee; // Forward declaration it tells the compilare that Emplyee is a class and declaration is elsewhere !  
class Project {
public:
	std::shared_ptr<Employee> m_emp;
	Project() {
		std::cout << "Project()" << std::endl;
	}
	~Project() {
		std::cout << "~Project()" << std::endl;
	}
};

class Employee {
public:
	//std::shared_ptr<Project> m_prj;   IF WE SHOULD USE SHARED POINTER HERE IT WILL CAOUSE MEMORY LEAK, BECAUSE WE HAVE A CIRUCLAR REFERENCE BETWEEN PROJECT AND EMPLOYEE INSTEAD
	// USE WEAK POINTER ON ONE OF THEM INSTEAD ! 
	std::weak_ptr <Project> m_prj;
	Employee() {
		std::cout << "Employee()" << std::endl;
	}
	~Employee() {
		std::cout << "~Employee()" << std::endl;
	}
};


int main() {
	std::shared_ptr<Employee> emp{ new Employee{} };
	std::shared_ptr <Project> prj{ new Project{} };

	emp->m_prj = prj;
	prj->m_emp = emp;

	return 0;
}


