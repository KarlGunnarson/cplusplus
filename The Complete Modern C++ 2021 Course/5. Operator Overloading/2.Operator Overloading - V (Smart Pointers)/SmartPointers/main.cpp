#include "Integer.h"
#include <iostream>
#include <memory>

// Resource Acquisition Is Initialization 
class IntPtr {
	Integer* m_p;
public:
	IntPtr(Integer* p) :m_p(p) {

	}
	~IntPtr() {
		delete m_p;
	}
	// Operator overloading to acces the underlying object integer. 
	Integer* operator ->() {
		return m_p;
	}

	Integer& operator *() {
		return *m_p;
	}
};
/*
Smart pointers should be used instead of raw pointers because the have the build in delete of resoruce and prevent memory leaks
*/
void Process(std::shared_ptr<Integer> ptr) {
	std::cout << "shared_ptr<Integer>" << std::endl;
	std::cout << ptr->GetValue() << std::endl;
}

void Process(std::unique_ptr<Integer> ptr) {
	std::cout << "unique_ptr<Integer>" << std::endl;
	std::cout << ptr->GetValue() << std::endl;
}

void CreateIntegerWithSharedSmartPointer()
{
	std::shared_ptr<Integer> p(new Integer); //shared pointer it used when want to share can be copied. it has internal reference count when the count is zero it will be destroyed.
	(*p).SetValue(69);
	Process(p);
	std::cout << (*p).GetValue() << std::endl;
	(*p).SetValue(80);
	std::cout << (*p).GetValue() << std::endl;
}
void CreateIntegerWithUniqueSmartPointer() 
{
	std::unique_ptr<Integer> p(new Integer); //unique pointer can never be copied  but it can be moved.
	(*p).SetValue(69);
	Process(std::move(p)); // After move it rescorese belong to Process and can not be accsedes anymore ! 
	//std::cout << (*p).GetValue() << std::endl;
}
void CreateIntegerDIYSmartPointer() {
	IntPtr p = new Integer;
	(*p).SetValue(19);
	std::cout << (*p).GetValue() << std::endl;
}
void CreateIntegerRawPointer() {
	Integer* p = new Integer;				// Create Integer object on the heap
	p->SetValue(3);
	std::cout << p->GetValue() << std::endl;
	delete p;								// Important to delete p to prevent memory leaks. 
}

int main() {
	CreateIntegerWithSharedSmartPointer();
	CreateIntegerWithUniqueSmartPointer();
	CreateIntegerDIYSmartPointer();
	CreateIntegerRawPointer();
	return 0;

}