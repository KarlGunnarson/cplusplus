#include <iostream>
#include "Integer.h"
/*


In General casting should be avoided ! 


*/


void Print(Integer a) {

}

int main() {
	/*
		TYPE CONVERSION PART 1 
	*/
	int a = 500, b = 200;
	float f = a;		// Compiler will inovke implicity cast 
	float f2 = a / b;	// implicity cast will not give the correct result of the division.

	/*
	C-Style cast should be avoided in C++ becuase it dont check the validness of the operation on can create bugs
	C-Slyte cast example below 
	*/


	float f2cStyle = (float)a / b;	// implicity cast will not give the correct result of the division.
	std::cout << f << std::endl;
	std::cout << f2 << std::endl;

	std::cout << "C-Style cast " << f2cStyle << std::endl;


	/*
	C++ Static cast is superior to c-style cast.
	*/



	float f4= static_cast<float>(a) / b;	// implicity cast will not give the correct result of the division.

	std::cout << "C++ static cast "<< f4 << std::endl;

	// Type cast between diffrent types 
	char* p = reinterpret_cast<char*>(&a);
	std::cout << "reinterpret_cast " << p << std::endl;


	const int iValue= 10; 
	int* p1 = const_cast<int*>(&iValue);

	std::cout << "const_cast " << *p1 << std::endl;


	/*
	TYPE CONVERSION PART 2 Primitive to User-defined

	Construtors take part in type conversions implicity or explict
	*/

	Integer a1{ 5 };

	//Initialization
	Integer a2 = 5;



	//Assignment
	a1 = 10;
	

	/*
	TYPE CONVERSION PART 3 User-defined to primitive 
	*/

	int x = static_cast<int>(a1);

	std::cout << x << std::endl;

	return 0;
}