#include "Integer.h"
#include <iostream>

Integer Add(const Integer& a, const Integer& b) {
	Integer temp;
	temp.SetValue(a.GetValue() + b.GetValue());
	return temp;
}


// Global operator overloading ! 
Integer operator +(int x, const Integer& y) {
	Integer temp;
	temp.SetValue(x + y.GetValue());
	return temp;
}

// Global operator overloading ! 
std::ostream& operator <<(std::ostream& out, const Integer& a) {
	out << a.GetValue();
	return out;
}

// Global operator overloading ! 
std::istream& operator >>(std::istream& input, Integer& a) {
	int x;
	input >> x;
	*a.m_pInt = x;
	return input;
}

int main() {
	Integer a(100), b(3);
	Integer sum1 = a + 5; //a.operator+(5)
	Integer sum2 = 100 + a;

	std::cout << sum1 << std::endl;
	std::cout << sum2 << std::endl;
	std::cin >> a;

	std::cout << a << std::endl;
	return 0;

}