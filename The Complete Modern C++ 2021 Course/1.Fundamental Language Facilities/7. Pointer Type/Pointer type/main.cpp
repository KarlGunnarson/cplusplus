#include <iostream>
int main()
{
	using namespace std;
	int x{ 10 };
	cout << &x << "\n"; // Print the adress of the variable
	int *ptr{ &x };
	cout << ptr << "\n";
	

	/*
	When creating a pointer it should match the source..
	*/

	/*
	When you want to point to diffrent types you can create a "void pointer"
	*/

	void* ptr1{ &x };
	cout << ptr1 << "\n";


	*ptr = 5;    // modify the value the pointer points to in this case variable x
	cout << x << "\n" ;

	int y{ *ptr }; // Initialze variable of x with the pointer.

	cout << y << "\n";

	// Null pointer
	// Should not write o read to null pointer.
	void *ptr2{ nullptr };
	return 0;
}