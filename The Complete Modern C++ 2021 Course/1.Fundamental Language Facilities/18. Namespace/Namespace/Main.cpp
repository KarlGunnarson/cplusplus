#include <iostream>

namespace Avg 
{
	float F_Calculate(float x, float y)
	{
		return (x + y) / 2;
	}
}


namespace Basic
{
	float F_Calculate(float x, float y)
	{
		return (x + y);
	}
}

namespace Sort 
{
	void Quicksort()
	{

	}

	void Insertionsort()
	{

	}

	void Mergesort()
	{

	}
	namespace Comparision		//	NESTED NAMESPACE
	{
		void Less()
		{

		}

		void Greater()
		{

		}
	}

}


// Namespace without a name
// denna kan inte anv�ndas i source files.
namespace
	{
		void InternalFunction()
		{

		}
	}
int main()
{

	/*
	Option 1 
	Avg::F_Calculate;
	

	Option 2
	using Avg::F_Calculate;

	Option 3 
	using namespace Avg;

	|- - - NESTED NAMESPACE - - - 
	Option 1 
	Sort::Comparision::Greater;
	
	
	Option 2 
	using namespace Sort::Comparision;
	*/
	InternalFunction();
	float fResult{ Basic::F_Calculate(34.45f,20.45f) };
	std::cout <<"addtion " << fResult << std::endl;
	float fResult2{ Avg::F_Calculate(10.0f,10.0f) };
	std::cout << "average " << fResult2 << std::endl;
	Sort::Comparision::Greater;
	Sort::Mergesort;
	return 0;
}