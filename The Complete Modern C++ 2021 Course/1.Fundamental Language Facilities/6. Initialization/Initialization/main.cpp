// Initialization 
#include <iostream>

int main()
{
/*

			AVOID COPY INITIALIZATION

*/


	
/*
	C++ 98 HOW TO INITIALIZE TYPES 
*/
	int a1;			// Uninitialized
	int a2 = 0;		// Copy initialization
	int a3(5);		// Direct initialization
	std::string s1;
	std::string s2("C++"); // Direct initialization

	char d1[8];		// Uninitialized
	char d2[8] = { '\0' }; // Copy initialization
	char d3[8] = { 'a','b','c','d' };	// Aggregate initialization // Copy initialization
	char d4[8] = { "ABCD" };// Copy initialization

	

	

/*
	UNIFORM INITIALIZATION INTRODUCED IN C++11
*/

	int b1{}; // Value initialization with default value.
	int b3{ 5 };// Direct initialization
//	int b2(); // Callin the function b2  "Most vexing parse"

	char e1[8]{};			// Array Value initialization with default value.
	char e2[8]{"Hello"};	// Array Direct initialization


	int* p1 = new int;// Uninitialized
	int* p2 = new int{};// Uniform initialization 

	char* p3 = new char[8]{}; // Value initialization
	char* p4 = new char[8]{ "Hello" };

	// Narrow conversions 
	//	float f{};
	//	int i{ f };


	/*
	1. Value initialization => T obj{};
	2. Direction initialization => T obj{v};
	3. Copy Initialization => T obc=v; AVOID !!
	*/

	/*
	Advantages of uniform initialization 
	1. It forces initialization 
	2. You can use direct initialization for array types 
	3. It prevents narrowing conversions 
	4. Uniform syntax for all types.
	*/


}