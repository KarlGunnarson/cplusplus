#include <iostream>

inline int Square(int x)
{
	return x * x;
}

int main()
{
	using namespace std;
	int result{ Square(5) };
	cout << result << endl;
}