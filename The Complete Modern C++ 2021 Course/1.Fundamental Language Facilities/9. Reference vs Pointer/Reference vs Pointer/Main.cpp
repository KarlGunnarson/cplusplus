#include <iostream>
#include "Generic.h"

int main()
{
	using namespace std;
	int a{ 5 }, b{ 10 }, c{ 88 };
	cout << "a= " << a << endl;
	cout << "b= " << b << endl;
	swap_ptr(&a, &b); // Call pointer based function with swap variables 
	cout << "swap" << endl;
	cout << "a= " << a << endl;
	cout << "b= " << b << endl;
	swap_ref(a, b);// Call ref based function with swap variables 
	cout << "swap" << endl;
	cout << "a= " << a << endl;
	cout << "b= " << b << endl;


	PrintPointer(&c); // call function print with adress of c
	PrintRef(c);


	return 0;
}