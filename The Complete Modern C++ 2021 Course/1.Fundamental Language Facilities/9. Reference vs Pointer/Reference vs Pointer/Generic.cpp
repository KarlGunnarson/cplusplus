#include <iostream>
// Function with pointer solution to swap variables 
void swap_ptr(int *x, int *y)
{
	int temp{ *x };
	*x = *y;
	*y = temp;
}

// Function with reference to swap variables prefer this solution  it is easier to read.
void swap_ref(int &x, int &y)
{
	int temp{ x };
	x = y;
	y = temp;
}

// Function with pointer solution to print the variable in the consonle window 
void PrintPointer(int *ptr)
{
	using namespace std;
	if (ptr != nullptr)
	{
		cout << *ptr << "\n";
	}
	else
	{
		cout << "null" << "\n";
	}
	
}// Function with pointer solution to print the variable in the consonle window 
void PrintRef(int &ref)
{
	using namespace std;
	cout << ref << "\n";
}
