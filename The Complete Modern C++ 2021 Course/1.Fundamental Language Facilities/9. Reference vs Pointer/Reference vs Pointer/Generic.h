#pragma once
void swap_ptr(int* x, int* y);
void swap_ref(int& x, int& y);
void PrintPointer(int* ptr);
void PrintRef(int& ref);