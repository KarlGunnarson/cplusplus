#include <iostream>

// it is possible to create to functions with the same name but with diffrent types...


int add(int a, int b)
{
	return a + b;
}

double add(double a, double b)
{
	return a + b;
}


float add(float a, float b)
{
	return a + b;
}


void Print(int x)
{

}

void Print(float x)
{

}


void Print(const int* x)
{

}

int main()
{
	using namespace std;
	
	cout << add(100, 100) << endl;     // This call the function on line 6 

	auto result{ add(3.1,3.1) };
	cout << result << endl;  // This call the function on line 11 
	return 0;
}