#include <iostream>

void Print(int count, char ch)
{
	using namespace std;
	for (int i{ 0 }; i < count; i++)
		cout << i <<" : " << ch << endl;
}

void endmessage()
{
	using namespace std;
	cout << "End of program " << endl;
}
int main()
{
	atexit(endmessage);    // ATEXIT KOMMER ALLTID ATT K�RAS I SLUTET AV PROGAMET �VEN IFALL DET LIGGER H�GST UPP
	Print(10,'g');		// VANDLIGT FUNCTION CALL
	void(*pfn)(int, char) {}; // FUNCTION POINTER MED NAMN pfn  KAN LIKA G�RNA D�PA DEN TILL BANAN.
	(*pfn)(8, '@');			// ALTERNATIV 1 ATT ANROPA EN FUCNTION POINTER 
	pfn(10, 'K');			// ALTERNATIV 2 ATT ANROPA EN FUCNTION POINTER 
	using namespace std;
	cout << "End of main" << endl;
	return 0;

}