#include <iostream>

/*
1. Create a variable that is constant 
2. Value cannot be modified 
3. Attempt to modify will cause compilation error
4. Replaces C macros
5. Commonly used with references
6. Const is declaried with upper case letters
*/

void PrintPointer(const int *ptr)
{
	using namespace std;
	cout << *ptr << endl;
}


void PrintReference(const int &ref)
{
	using namespace std;
	cout << ref << endl;
}



int main()
{
	
	using namespace std;
	const float PI{ 3.14159f };
	float radius{};
	cout << "Please type in the radius: ";
	cin >> radius;
	float area{ PI * radius * radius };
	float circumference{ PI * 2 * radius };
	cout << "Area is : "<< area << endl;
	cout << "Circumference is : " << circumference << endl;
	
	
	
	const int CHUNK_SIZE{ 512 };
	const int* const ptr{ &CHUNK_SIZE };
	int x{ 10 };

	PrintPointer(&x);
	PrintReference(CHUNK_SIZE);
	return 0;
}