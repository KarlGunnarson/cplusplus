#include <iostream>

/*

Implement the following functions using pointer arguments only

*/

int Add(int* a, int* b)//Add two numbers and return the sum
{
	return *a + *b;
}	
void AddVal(int* a, int* b, int* result) //Add two numbers and return the sum through the third pointer argument
{
	*result = *a + *b;
}
void Swap(int* a, int* b)//Swap the value of two integers
{
	int temp{*b};
	*b = *a;
	*a= temp;
}

void Factorial(int* a, int* result) //Generate the factorial of a number and return that through the second pointer argument
{
	*result = 1;
	for (int i{ 1 }; i <= *a; i++)
	{
		*result = *result * i;
	}
}



int main()

{
	/*std::cout << "return the sum with pointer" << std::endl;
	int value1{ 10 };
	int value2{ 20 };
	int result{};

	int *ptr1{ &value1 };
	int	*ptr2{ &value2 };
	int	*ptr3{ &result };
	
	*ptr3 = Add(ptr1, ptr2);
	std::cout << *ptr3 << std::endl;

	value1 = 20;
	std::cout << "return the sum with pointers in result" << std::endl;
	AddVal(ptr1, ptr2, ptr3);
	std::cout << *ptr3 << std::endl;

	value1 = 9;
	std::cout << "Swap two values" << std::endl;
	std::cout << "value1 : " << value1 << std::endl;
	std::cout << "value2 : " << value2 << std::endl;
	Swap(ptr1, ptr2);
	std::cout << "Swapped values" << std::endl;
	std::cout << "value1 : " << value1 << std::endl;
	std::cout << "value2 : " << value2 << std::endl;


	std::cout << "calculate the factorial value of "<< *ptr2 << std::endl;
	int result5{0};
	int* ptr4{ &result5 };
	Factorial(ptr2, ptr4);
	std::cout << "Result is " << *ptr4 << std::endl;

	
	Write the following code & do as instructed in the comments. Build and observe.
	
	*/
	//Try to modify x1 & x2 and see the compilation output
	int x = 5;
	const int MAX = 12;
	int& ref_x1 = x;
	const int& ref_x2 = x;

	ref_x1 = 99;

	//ref_x2 = &value1;

	//Try to modify the pointer (e.g. ptr1) and the pointee value (e.g. *ptr1)
	const int* ptr1 = &x;
	int* const ptr2 = &x;
	const int* const ptr3 = &x;


	
	//ptr30 = &value1;


	//Find which declarations are valid. If invalid, correct the declaration
	//const int* ptr3 = &MAX;
	const int* ptr4 = &MAX;

	int& r1 = ref_x1;
	const int& r2 = ref_x2;

	const int*& p_ref1 = ptr1;
	int* const & p_ref2 = ptr2;
	return 0;
}