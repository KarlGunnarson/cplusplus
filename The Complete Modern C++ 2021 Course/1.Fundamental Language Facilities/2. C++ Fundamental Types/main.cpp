// Initial values  
// test 
/*
test
*/

#include <iostream>

int main()
{
	using namespace std;
	//Scalar types
	int		i		=0;
	char	ch		= 'a';
	bool	flag	= true;
	float	f		= 1.234f;
	double	d		= 5232.333;
	
	//Vector types 
	int arr[5];
	int arr1[5] = { 1,2,3,4,5, };


	cout << "i = " << i << endl;
	cout << "ch = " << ch << endl;
	cout << "flag = " << flag << endl;
	cout << "f = " << f << endl;
	cout << "d = " << d << endl;
	cout << "arr = " << arr << endl;
	cout << "arr1[0] = " << arr1[0] << endl;
	cout << "arr1[1] = " << arr1[1] << endl;
	cout << "arr1[2] = " << arr1[2] << endl;
	cout << "arr1[3] = " << arr1[3] << endl;
	cout << "arr1[4] = " << arr1[4] << endl;
	cout << "arr1[5] = " << arr1[5] << endl;
	return 0;

}

