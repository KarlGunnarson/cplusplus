#include <iostream>

int main()
{
	using namespace std;
	int arr[]{ 1,2,3,4,5};


	// Single loop
	for (int i{ 0 }; i < 5; i++)
	{
		cout << arr[i] << " Single loop " << endl;
	}

	// range based loop
	for (int x :arr)
	{
		cout << x << " Range based loop "<< endl;
	}

	// range based loop with auto declaration
	// Program copy from arr
	for (auto p : arr)
	{
		cout << p << " Auto - Range based loop " << endl;
	}

	// range based loop with auto reference to the array
	// in this case we can modify the values in the array
	for (auto& a : arr)
	{
		cout << a << " Auto ref - Range based loop " << endl;
		a = 99;
		cout << a << " Auto ref - Range based loop has been modifed in the loop " << endl;
	}


	// range based loop with constant referenct to the array
	// it prevet from modify the values in the array
	int arr2[]{ 1,2,3,4,5,6,7,8,9 };
	for (auto const& c : arr2)
	{
		cout << c << " Auto const ref - Range based loop " << endl;
		
	}


	//auto initialas a list 
	for (auto l : { 1,2,3,4,5,6 })
	{
		cout << l << " Auto initiliaze a list and range based loop " << endl;
	}


	//	WHILE LOOP 
	// Pointer to start & end of the array
	int* beg{ &arr2[0] };
	int* end{ &arr2[5] };

	while (beg != end)
	{
		cout << *beg << " While loop  " << endl;
		++beg;
	}


	//	WHILE LOOP 
	// With build in fucntion in c++ to determinate start and end
	int* beg2{ std::begin(arr2) };
	int* end2{ std::end(arr2) };

	while (beg2 != end2)
	{
		cout << *beg2 << " While loop with build in c++ functions  " << endl;
		++beg2;
	}

	// How the range based for loop works internaly

	auto&& range{ arr2 };
	auto begin3{ std::begin(arr2) };
	auto end3{ std::end(arr2) };


	for(;begin3 !=end3;++begin3)
	{
	
		auto v{ *begin3 };
		cout << v << " How range based loop work internaly  " << endl;

	}



}