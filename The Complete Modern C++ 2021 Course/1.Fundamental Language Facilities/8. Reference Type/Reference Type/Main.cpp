#include <iostream>
int main()
{
	using namespace std;
	int x{ 10 };	// Referent 
	int &ref{ x };	// Reference
	int y{ 1000 };

	ref = y;


	cout << "REFERENCE" <<  endl;
	cout << "x value - " << x << endl;
	cout << "ref value - " << ref << endl;
	cout << "x adr - " << &x << endl;
	cout << "ref adr - " << &ref << endl;
	return 0;
}