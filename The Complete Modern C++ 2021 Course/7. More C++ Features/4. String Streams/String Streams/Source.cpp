#include <iostream>
#include <sstream>
#include <typeinfo>

int main() {

	int a{ 5 }, b{ 6 };
	int sum{ a + b };
	//std::cout << "Sum of " << a << " & " << b << " is : " << sum << std::endl;
	//std::string output = "Sum of " + a + " & " + b + " is " + sum; This line cannot work to convert int to string. we must use the class stringstream to solve this.

	//String stream classes
	std::stringstream ss;	//Both read & write
	std::istringstream is;  // Extration only read
	std::ostringstream os;  // Insertion only write

	ss << "Sum of " << a << " & " << b << " is : " << sum << std::endl;
	std::string s{ ss.str() };
	std::cout << s << std::endl;
	ss.str(""); // Clear the string buffer 
	ss << sum;
	auto ssum{ std::to_string(sum) };
	std::cout << ssum << std::endl;
	std::cout << typeid(ssum).name() << std::endl;
	std::cout << typeid(sum).name() << std::endl;


	// Convert string number to numbers
	std::string data{ "12 89 21" };
	int c;
	std::stringstream ssData;
	ssData.str(data);

	while (ssData >> c)	{ // extract from ssData to int var c
		std::cout << c << std::endl;
	}

	//convert string to int
	int x{ std::stoi("584") };
	//convert string to float
	float ff{ std::stof("23423.1") };

	std::cout << x << std::endl;

	std::cout << ff << std::endl;


	return 0;
}