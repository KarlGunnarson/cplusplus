#include <iostream>

// UNSCOPE ENUMRATED ! 
enum WordContitent{SWEDEN,USA,GERMANY,DUBAI};


// prefer to use SCOPE enum instead of UNSCOPE enumon to prevent name conflicts ! 
enum class Color{RED,GREEN,BLUE}; // scope enum as default the underlaying type is int
enum class TrafficLight:char{RED='R', GREEN = 'G', BLUE = 'B'};


void FillColor(Color color) {
	// Full background with some color
	switch (color)
	{
	case Color::RED:
		std::cout << "Background - RED" << std::endl;
		break;
	case Color::GREEN:
		std::cout << "Background - GREEN" << std::endl;
		break;
	case Color::BLUE:
		std::cout << "Background - BLUE" << std::endl;
		break;
	}

}

void SetTrafficLight(TrafficLight trafficlight) {
	switch (trafficlight)
	{
	case TrafficLight::RED:
		std::cout << "TrafficLight - RED" << std::endl;
		break;
	case TrafficLight::GREEN:
		std::cout << "TrafficLight - GREEN" << std::endl;
		break;
	case TrafficLight::BLUE:
		std::cout << "TrafficLight - BLUE" << std::endl;
		break;
	default:
		break;
	}
	
}

void EnterWorld(WordContitent wordcontitent) {
	switch (wordcontitent)
	{
	case SWEDEN:
		std::cout << "Welcome to Sweden" << std::endl;
		break;
	case USA:
		std::cout << "Welcome to USA" << std::endl;
		break;
	case GERMANY:
		std::cout << "Welcome to Germany" << std::endl;
		break;
	case DUBAI:
		std::cout << "Welcome to Dubai" << std::endl;
		break;
	default:
		break;
	}
}


int main() {
	// 3 options to use enum
	Color c{ Color::RED };
	FillColor(c);						
	FillColor(Color::GREEN);
	FillColor(static_cast<Color>(2));
	SetTrafficLight(TrafficLight::GREEN);
	EnterWorld(SWEDEN);
	EnterWorld(USA);

}