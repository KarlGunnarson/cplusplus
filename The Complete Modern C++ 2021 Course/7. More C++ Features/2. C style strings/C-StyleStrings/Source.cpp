#include <iostream>
#include <cstring>


const char* Combine(const char* pFirst, const char* pLast) {
	// Dynamic memory allocation
	// Allocate memory on the heap ! 
	// IN C THE STRING ARE NULL TERMINATED YOU NEED TO ADD ONE EXTRA BYTE WHEN ALLOCATE MEMORY
	char* fullname = new char[strlen(pFirst) + strlen(pLast)+1];
	//char fullname[20];
	strcpy(fullname, pFirst);
	strcat(fullname, pLast);
	return fullname;
}


int main() {
	char first[10];
	char last[10];
	std::cin.getline(first,10);
	std::cin.getline(last, 10);

	const char* pFullName = Combine(first, last);
	// Insert into the database 

	std::cout << pFullName << std::endl;


	//Delete memomry
	delete[] pFullName;


	return 0;

}