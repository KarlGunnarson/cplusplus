#include <iostream>
#include <cstring>
#include <string>

/*

C-STYLE ! 

*/
const char* Combine(const char* pFirst, const char* pLast) {
	// Dynamic memory allocation
	// Allocate memory on the heap ! 
	// IN C THE STRING ARE NULL TERMINATED YOU NEED TO ADD ONE EXTRA BYTE WHEN ALLOCATE MEMORY
	char* fullname = new char[strlen(pFirst) + strlen(pLast)+1];
	//char fullname[20];
	strcpy(fullname, pFirst);
	strcat(fullname, pLast);
	return fullname;
}

void UsingstdString() {
	// Initialieze & assing
	std::string s{ "Hello" };

	//Access
	s[0] = 'W';
	char ch{ s[0] };

	std::cout << s << std::endl;
	std::cin >> s;

	std::getline(std::cin, s);
	 // Rawstring
	printf("%s", s.c_str());

	// Size 
	s.length();

	// Insert and concatente
	std::string s1{ "Hello" }, s2{ "World" };
	s = s1 + s2;
	s += s1;

	s.insert(6, "World");

	// Comparsion
	if (s1 != s2) {

	}

	//removal
	s.erase();
	s.erase(0, 5);
	s.clear();

	// Search
	auto pos = s.find("World", 0);
	if (pos != std::string::npos) {

	}


}


int main() {
	/*

	C-STYLE !

	*/
	char first[10];
	char last[10];
	std::cin.getline(first,10);
	std::cin.getline(last, 10);

	const char* pFullName = Combine(first, last);
	// Insert into the database 

	std::cout << pFullName << std::endl;


	//Delete memomry
	delete[] pFullName;

	/*
	
	C++ - STYLE 
	 
	 Prefer to use std::string instead of raw sting
	*/

	std::string first2;
	std::string last2;

	std::getline(std::cin, first2);
	std::getline(std::cin, last2);

	std::string fullname2{ first2 + last2 };

	std::cout << fullname2 << std::endl;




	return 0;

}