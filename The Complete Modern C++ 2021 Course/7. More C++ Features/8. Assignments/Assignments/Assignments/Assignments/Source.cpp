#include <iostream>

// Function convert complete string to lower case
std::string ToLower(const std::string& str){
	char* ch{ new char[str.size()] };
	
	std::string sConverted{ str };
	
	for (int i = 0; i < str.size(); i++)
	{
		ch[i] = str[i];
		sConverted[i] = tolower(ch[i]);
	}
	return sConverted;
}

// Function convert complete string to upper case
std::string ToUpper(const std::string& str) {
	char* ch{ new char[str.size()] };

	std::string sConverted{ str };

	for (int i = 0; i < str.size(); i++)
	{
		ch[i] = str[i];
		sConverted[i] = toupper(ch[i]);
	}
	return sConverted;
}


enum class Case { SENSITIVE, INSENSITIVE };

size_t Find(
	const std::string& source,         //Source string to be searched
	const std::string& search_string,  //The string to search for
	

	Case searchCase = Case::INSENSITIVE,//Choose case sensitive/insensitive search
	size_t offset = 0)//Start the search from this offset

{                
	
										
	if (searchCase ==Case::INSENSITIVE)
	{
		//Temp 
		std::string sTempSource{ ToLower(source) };
		std::string sTempSearch{ ToLower(search_string) };
		if (sTempSource.find(sTempSearch, offset) != sTempSource.npos) //
		{
			return sTempSource.find(sTempSearch, offset);
		}
		else
		{
			return -1;
		}

	}
	else if (searchCase == Case::SENSITIVE)
	{
		if (source.find(search_string, offset) != source.npos) //
		{
			return source.find(search_string, offset);
		}
		else
		{
			return -1;
		}
	}
		//Implementation

		/*
		return position of the first character
		of the substring, else std::string::npos
		*/
	
}


int main() {
	std::string sTest{ "HELLO" };
	ToLower(sTest);
	std::string sTest1{ "you" };
	ToUpper(sTest1);

	const std::string source{"kArL GuNnArSsOn"};
	const std::string search_string{"gunnarsson"};


	std::cout << Find(source, search_string, Case::INSENSITIVE, 0) << std::endl;

	return 0;
}

