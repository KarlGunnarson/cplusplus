#include <iostream>
constexpr int GetNumber() {
	return 50;
}

constexpr int add(int x, int y) {
	return x + y;
}


// if statement comes in c++ 17 
constexpr int Max(int x, int y) {
	if (x > y)
		return x;
	return y;
}

int main() {
	/*
	# Represents an expression that is constant
	# Such expressions are possibly evaluated at compile time
	# Can be applied to variable declarations or functions
	# May increase the performance of the code as computation is done at compile time.
	*/


	// behaves as a constexpr function
	constexpr int i{ GetNumber() }; // Evaluate att compile time
	int arr[i];

	
	// behaves as a constexpr function
	const int j{ GetNumber() }; // Evaluate att compile time
	int arr1[j];


	//Behaves as a normal function.
	int x{ GetNumber() };


	int sum{ add(10,45) };

	return 0;
}

/*constanct expressions function rules

1. Should accept and return literal types only
	(void,scalar types(int,float,char), references, etc ....)

2.Should contain only single line statement that should be a return statement

3. constexpr functions are implicity inline

*/