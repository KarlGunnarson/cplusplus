#include <iostream>
#include <initializer_list>;
#include <cassert>


// COTAINER CLASS HOLD OBJECTS OF OTHER CLASSES 
class Bag {
	int arr[10];
	int m_Size{};
public:
	Bag(std::initializer_list<int>values) {
		assert(values.size() < 10);
		auto it{ values.begin() };
		while (it != values.end()) {
			add(*it);
			++it;
		}
	}
	void add(int value) {
		assert(m_Size < 10);
		arr[m_Size++] = value;
	}

	void Remove() {
		--m_Size;
	}

	int operator [](int index) {
		return arr[index];
	}

	int GetSize()const {
		return m_Size;
	}
};


void Print(std::initializer_list<int>values) {
	//auto it{ values.begin() };
	//while (it != values.end()) {
	//	std::cout << *it++ << " ";
	//}

	for (auto x : values) { // use range based loop
		std::cout << x << " ";
	}
}

int main() {

	// Standard uniform initialstion
	int x{ 0 };
	float y{ 3.1f };
	int arr[5]{ 0,1,2,3,4 };
	std::string s{ "Hello C++" };


	
	std::initializer_list<int> data = { 1,2,3,4 };
	auto values = { 1,2,3,4 };

	Bag b{1,2,3,4,5,6,7}; // Initilisze list for used define object
	//b.add(3); old typeing
	//b.add(2);
	//b.add(1);

	for (int i = 0; i < b.GetSize(); ++i) {
		std::cout << b[i] << " ";
	}

	Print({ 345433453,2,24,534,543534,543,534,53, });
	return 0;
}