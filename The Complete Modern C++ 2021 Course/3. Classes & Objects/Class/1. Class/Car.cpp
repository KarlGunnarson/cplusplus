#include "Car.h"
#include <iostream>

Car::Car()
{
	std::cout << "Car();" << std::endl;
	fuel = 0;
	speed = 0;
	passagengers = 0;
}

Car::Car(float amount)
{
	fuel = amount;
	speed = 0;
	passagengers = 0;
}

Car::~Car()
{
	std::cout << "~Car();" << std::endl;
}

void Car::FillFuel(float amount)
{
	fuel = amount;
}

void Car::Accelerate()
{
	speed++;
	fuel -= 0.5f;
}

void Car::brake()
{
	speed = 0;
}

void Car::AddPassengers(int count)
{
	passagengers = count;
}

void Car::DashBoard()
{
	std::cout << "Fuel:" << fuel << std::endl;
	std::cout << "Speed:" << speed << std::endl;
	std::cout << "Passengers:" << passagengers << std::endl;

}