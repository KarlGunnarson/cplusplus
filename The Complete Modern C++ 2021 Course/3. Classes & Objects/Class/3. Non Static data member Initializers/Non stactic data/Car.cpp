#include "Car.h"
#include <iostream>

int Car::totalCount{ 0 };
Car::Car()
{
	++totalCount;
	std::cout << "Car();" << std::endl;

}

Car::Car(float amount)
{
	++totalCount;
	fuel = amount;

}

Car::~Car()
{
	--totalCount;
	std::cout << "~Car();" << std::endl;
}

void Car::FillFuel(float amount)
{
	fuel = amount;
}

void Car::Accelerate()
{
	speed++;
	fuel -= 0.5f;
}

void Car::brake()
{
	speed = 0;
}

void Car::AddPassengers(int count)
{
	passagengers = count;
}

void Car::DashBoard()
{
	std::cout << "Fuel:" << fuel << std::endl;
	std::cout << "Speed:" << speed << std::endl;
	std::cout << "Passengers:" << passagengers << std::endl;
}

void Car::ShowCount()
{
	std::cout <<"Total count: " << totalCount << std::endl;
}
