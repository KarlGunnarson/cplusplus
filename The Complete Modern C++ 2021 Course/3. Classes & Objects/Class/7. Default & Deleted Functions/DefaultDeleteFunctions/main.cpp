#include <iostream>
class Integer {
	int m_Value{ 0 };
public:
	Integer() = default;
	Integer(int value) {
		m_Value = value;
	}

	Integer(const Integer &) = delete; // this line prevent that the class Integer cannot be copied. !!! 

	void SetValue(int value) {
		m_Value = value;
	}

	void SetValue(float value) = delete; // This line prevent that the developer pass a floating piont during invode of this function!!
};

int main() {
	Integer i1;
	Integer i2;
	Integer i3;
	//Integer i43(i2);
	i1.SetValue(3);
	//i2.SetValue(3.4f);
	return 0;
}