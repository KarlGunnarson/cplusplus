#pragma once
class Car {
private:
	float fuel;
	float speed;
	int passagengers;
	static int totalCount;
public:
	Car(); // Constructor to set default values 
	Car(float amount);
	Car(float amount, int pass);
	~Car(); // Deconstrutor to release memory 
	void FillFuel(float amount);
	void Accelerate();
	void brake();
	void AddPassengers(int count);
	void DashBoard();
	void ShowCount();
};