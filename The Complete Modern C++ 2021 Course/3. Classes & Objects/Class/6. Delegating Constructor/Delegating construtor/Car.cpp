#include "Car.h"
#include <iostream>

int Car::totalCount{ 0 }; // Static variable init !!! 

Car::Car():Car(0) // Construtor  || invoke construtor Car:(amount);
{
	std::cout << "Car();" << std::endl;

}

Car::Car(float amount):Car(amount,0)// Construtor invoke construtor Car:(amount,Pass);
{
	std::cout << "Car(float);" << std::endl;
}

Car::Car(float amount, int pass)// Constrotur
{
	/*
	COMMON INTIAL CODE SHOULD ONLY BE IN ONE CONSTRUTOR !! TO AVOUD BUGS AND HARD READ CODE ! 
	*/
	std::cout << "Car(float,int);" << std::endl;
	++totalCount;
	fuel = amount;
	speed = 0;
	passagengers = pass;
}

Car::~Car()// De construtor
{
	std::cout << "~Car();" << std::endl;
}

void Car::FillFuel(float amount)
{
	fuel = amount;
}

void Car::Accelerate()
{
	speed++;
	fuel -= 0.5f;
}

void Car::brake()
{
	speed = 0;
}

void Car::AddPassengers(int count)
{
	passagengers = count;
}

void Car::DashBoard()
{
	std::cout << "Fuel:" << fuel << std::endl;
	std::cout << "Speed:" << speed << std::endl;
	std::cout << "Passengers:" << passagengers << std::endl;

}

void Car::ShowCount()
{
	std::cout << totalCount << std::endl;
}
