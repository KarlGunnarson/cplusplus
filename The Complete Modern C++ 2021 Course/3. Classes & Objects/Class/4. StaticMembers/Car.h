#pragma once
class Car {
private:
	float fuel{ 0 }; // Init here ! 
	float speed{ 0 };
	int passagengers{0};
	static int totalCount;
public:
	Car(); // Constructor to set default values 
	Car(float amount);
	~Car(); // Deconstrutor to release memory 
	void FillFuel(float amount);
	void Accelerate();
	void brake();
	void AddPassengers(int count);
	void DashBoard();
	static void ShowCount();
};