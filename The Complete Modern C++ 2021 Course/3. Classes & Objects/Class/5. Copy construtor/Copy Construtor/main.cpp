#include "Integer.h"
#include <iostream>;

int main() {
	int* p1 = new int(5);
	// Shallow copy | Only copies the adresses 
	int* p2 = p1;

	// Deep copy | Copes the value of the adress instead 
	int* p3 = new int(*p1);


	// Avoid to copy opbject but when you need you have to implement a copy constructor .!!!!! 
	Integer i(10);
	Integer i2(i);
	std::cout << i.GetValue();
	std::cout << i2.GetValue();
	return 0;

}