#pragma once
class Integer {
	int* m_pInt;
public:
	Integer();
	Integer(int value);
	Integer(const Integer &obj); // DEEP COPY  | copy constructor .!!!!! 
	int GetValue()const;
	void SetValue(int value);
	~Integer();
};
