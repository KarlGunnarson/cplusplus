#include <stdio.h>
#include <stdlib.h>

int main()
{
	// MALLOC FUNTION.... ALLOCATE RAW MEMORY ON THE HEAP
	/*
	This code works in C

	int* p = malloc(sizeof(int)); // Allocate memory for pointer p
	*p = 5; // Write value to pointer p
	printf("%d", * p); // print value to console.
	free(p); // free the allocated memory for pointer p
	p = NULL;
	return 0;
	
	*/

	// Need to type cast to c-style to have above code working in cpp.
	int* p = (int*)malloc(sizeof(int)); // Allocate memory for pointer p
	*p = 5; // Write value to pointer p 
	printf("%d", * p); // print value to console.
	free(p); // free the allocated memory for pointer p    IF WE FORGET TO FREE THE MEMORY IT WILL LEAD TO MEMORY LEAKS !! 
	p = NULL;

	// Allocate memory for array of 5 integers
	int* b{ (int*)malloc(5*sizeof(int)) };
	if (b == NULL)
	{
		printf("Failed to allocate memory\n");
	}

	*b = 10; // Write value to pointer p 
	printf("%d", *b); // print value to console.
	free(b); // free the allocated memory for pointer p 
	b = NULL;

	

	// CALLOC FUNCTION ... ALLOCATED MEMORY ON THE HEAP AND INITIALIZES IT TO ZERO
	int* k{ (int*)calloc(1,sizeof(int)) };
	if (k==NULL)
		{
		printf("Failed to allocate memory\n");
		}

	*k = 10; // Write value to pointer p 
	printf("%d", *k); // print value to console.
	free(k); // free the allocated memory for pointer p 
	k = NULL;


	// Allocate memory for array of 5 integers
	int* a{ (int*)calloc(5,sizeof(int)) };
	if (a == NULL)
	{
		printf("Failed to allocate memory\n");
	}

	*a = 10; // Write value to pointer p 
	printf("%d", *a); // print value to console.
	free(a); // free the allocated memory for pointer p 
	a = NULL;



	return 0;
}