#include <stdio.h>
#include <stdlib.h>
#include <iostream>

void Malloc()
	{
		int* p{ (int*)malloc(5 *sizeof(int)) };
		if (p == NULL)
		{
			printf("Failed to allocate memory\n");
		}
		*p = 5;
		std::cout << *p << std::endl;
		free(p);
		p = NULL;
	}


void New()
	{
		int* p = new int(5); // allocate memory for the pointer with new !! Can not specify the size of the variable like we can do in C // CAN INITIALZE A Value 
		*p = 69;
		std::cout <<*p << std::endl;
		delete p;
		p = nullptr;
	}

void NewArrays()
	{
		int* p = new int[5];
		for (int i{ 0 }; i < 5; i++)
		{
			p[i] = i;
		}
		delete[]p; // importat.
	}

void String()
{
	char* p{ new char[4] };
	strcpy_s(p, 4, "C++");
	std::cout << p << std::endl;
	delete[]p;
}

void TwoD()
{
	int* p1 = new int[3];
	int* p2 = new int[3]; 

	int** pData = new int* [2]; // array of integer pointers | pointer to pointer !
	pData[0] = p1;
	pData[1] = p2;

	pData[0][1] = 2;
	std::cout << pData[0] << std::endl;
	std::cout << pData[1] << std::endl;
	/*
	Delete command should be used in the order the memory has been allocated
	for every new operator a delete is needed..
	*/
	delete[]p1; // Delete []pData[0]
	delete[]p2; // Delete []pData[1]
	delete[]pData;
}
int main()
{
	int data[2][3] =
	{
		1,2,3,
		4,5,6
	};
	NewArrays();
	String();
	TwoD();
}