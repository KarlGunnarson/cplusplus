#include <iostream>

using namespace std;

int main()
{
    cout << "What do you want to eat? " << endl;
    string sFood;
    getline(cin,sFood);
    cout << "What do you want to drink? " << endl;
    string sDrink;
    getline(cin,sDrink);
    cout << "What is the price of the food? " << endl;
    double iPrizeFood;
    cin>>iPrizeFood;

    cout << "What is the price of the drink " << endl;
    double iPrizeDrink;
    cin>>iPrizeDrink;

    cout << "What is the tip you want to leave in % ? " << endl;
    double iTip;
    cin>>iTip;

    double iPrizeTip = (iPrizeFood+iPrizeDrink) * (iTip/100);
    double iTotalPrize = iPrizeFood+iPrizeDrink +iPrizeTip;

    cout << "Your order is " << sFood << " and " << sDrink <<" as a drink " << " the total price of your order with the tip is " << iTotalPrize << " dollars ! " <<endl;
    return 0;
}
