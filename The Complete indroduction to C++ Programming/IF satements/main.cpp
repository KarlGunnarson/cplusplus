#include <iostream>
// if(value1=="yes" || value2 > 4) OR STATEMENT
//  if(value1=="yes" && value2 > 4) AND STATEMENT
using namespace std;

int main()
{
    string value1   = "no";
    int value2      =5;
    int value3      =6;

    if(value1=="yes" || value2 > 4 && value3 ==6)
        {
        cout<<"Great"<<endl;
        }
    else
        {
         cout<<"Bad"<<endl;
        }
    return 0;
}
